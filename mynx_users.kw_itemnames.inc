<?php

/**
 * Implements hook_kw_itemnames_type_info().
 */
function mynx_users_kw_itemnames_type_info() {
  $result = array();

  $result['user'] = array(
    'item load callback' => 'user_load',
    'item create callback' => 'mynx_users_item_create',
    'item update callback' => 'mynx_users_item_update',
    'item delete callback' => 'user_delete',
    'item extract id callback' => 'mynx_users_item_extract_id',
  );

  return $result;
}

function mynx_users_item_create($defaults, $required) {
  $user_properties = ($required + $defaults);

  return user_save('', $user_properties);
}

function mynx_users_item_update($user, $required) {
  return user_save($user, $required);
}

function mynx_users_item_extract_id($user) {
  return $user->uid;
}
