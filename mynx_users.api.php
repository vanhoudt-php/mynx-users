<?php

/**
 * Implement this hook to declare user roles to be created upon running
 * kraftwagen manifests.
 *
 * The hook is called in the callback defined for the "define_default_roles"
 * manifest (defined by this module in the .install file), called
 * mynx_users_manifest_define_default_roles().
 *
 * Return an info array describing the desired roles in the following structure:
 * array(
 *   "role name" => array(
 *     'weight' => 1,
 *     'permissions' => array(
 *       'permission system name',
 *       'another permission system name',
 *       ...
 *     ),
 *   ),
 *   "another role name => array(
 *     ...
 *   ),
 * )
 */
function hook_mynx_roles_info() {
  return array(
    'administrator' => array(
      'weight' => 1,
      'permissions' => array(
        'administer nodes',
        'administer users'
      ),
    ),
    'editor' => array(
      'weight' => 2,
      'permissions' => array(
        'edit any page',
      ),
    )
  );
}

function hook_mynx_roles_info_alter(&$roles_info) {

}

function hook_mynx_user_one_info_alter(&$user_one_info) {

}

function hook_mynx_users_info() {

}

function hook_mynx_users_info_alter(&$users_info) {

}
